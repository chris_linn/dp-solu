-- printing a table of squares directly
-- main ::IO ()
-- main = do
--     putStrLn "Table of squares:"
--     print_table 1 10

-- print_table :: Int -> Int -> IO ()
-- print_table cur max
--     | cur > max = return ()
--     | otherwise = do
--     putStrLn (table_entry cur)
--     print_table (cur+1) max


-- printing a table of squares indirectly
main = do
        putStrLn "Table of squares:"
        let row_actions = map show_entry [1..15]
        execute_actions (take 10 row_actions)

table_entry :: Int -> String
table_entry n = (show n) ++ "^2 = " ++ (show (n*n))

show_entry :: Int -> IO ()
show_entry n = do putStrLn (table_entry n)

execute_actions :: [IO ()] -> IO ()
execute_actions [] = return ()
execute_actions (x:xs) = do
                            x
                            execute_actions xs


-- table_entry :: Int -> String
-- table_entry n = (show n) ++ "^2 = " ++ (show (n*n))

-- testMonad :: IO ()
-- testMonad = putStrLn "1"
--     >>=
--     \_ -> return 1 --has to use return
--     >>=
--     \_ -> putStrLn "2"


test_if_else n =    if n>1.0
                        then n
                    else if n <0.0
                        then (-n)
                    else 0.0


test_case n = case n of
                1 -> show 1
                otherwise -> show (-1)

test_l_c :: [Int]
test_l_c = [x| x<-[1..5],x>3]

aaa a = if a=="true" then [True]
        else if a=="false" then [False]
        else [True,False]