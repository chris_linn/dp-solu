%% Q1 我觉得我突然明白了 tail recursive
sumlist(List, Sum) :-
    sumlist(List, 0, Sum).

sumlist([], Sum, Sum).
sumlist([N|Ns], Sum0, Sum) :-
    Sum1 is Sum0 + N,
    sumlist(Ns, Sum1, Sum).